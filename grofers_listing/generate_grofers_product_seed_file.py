import json
import sys

fp = open('grofers_product_seed_file.json', 'w+')
brand_list = ["Kinley", "HaveMore", "Haldiram's Nagpur", "Bingo Mad Angles", "Bisk Farm", "Grofers Mother's Choice", "Too Yumm", "Colgate MaxFresh", "Catch", "Patanjali", "Sunfeast Yippee", "Daawat", "Sensodyne", "Everest", "Britannia Good Day", "B Natural", "Naga", "Cadbury Nutties", "Grofers Happy Day", "GRB", "Real Fruit Power", "India Gate", "Maggi", "Paper Boat", "Nissin", "Tropicana 100%", "Karachi Bakery", "Duke's", "MTR", "Dabur", "Colgate", "Knorr", "Sunfeast", "Kurkure", "Vandevi", "Pepsodent", "Amul", "Oreo", "Tropicana", "Parle-G", "Parle Hide & Seek", "Sunfeast Dark Fantasy", "Kissan", "Balaji", "Chheda's", "Saffola", "Parle Monaco", "Bisleri", "Sunfeast Bounce", "Dabur Red", "Aquafina", "Fortune", "Amul Kool", "Britannia Nice Time", "Charlie's", "Gemini", "Mother Dairy", "Britannia Pure Magic", "Tata Sampann", "Lay's", "Sunfeast Farmlite", "Britannia Marie Gold", "Munch", "Top Ramen", "Haldiram's", "McCain", "Cadbury 5 Star", "Nature Fresh", "Cadbury Dairy Milk Silk", "Aashirvaad Select", "Dhara", "Real", "Cadbury Bournville", "Himalayan", "Britannia Bourbon", "Milkybar", "Real Activ", "Sunfeast Marie Light", "Fun Foods", "Sunrich", "Sweekar", "Britannia NutriChoice", "Sagar", "Puro", "Raw Pressery", "Parle BakeSmith", "Parle Milano", "KitKat", "Parle Krackjack", "Lehar", "What If", "Parle Hide & Seek Bourbon", "Manpasand", "Pickwick", "Colgate Sensitive", "Gowardhan", "Cadbury Dairy Milk", "Closeup", "Cadbury Bournvita", "Sunfeast Pasta Treat", "Aashirvaad", "Cadbury Fuse", "Snickers", "Himalaya", "Liberty", "Cadbury Celebrations", "Dabur Meswak", "Sunfeast Mom's Magic", "Cadbury Oreo", "Vijaya", "Tata", "Britannia 5050", "Best Value", "aavin", "Mukharochak", "Parle Melody", "Bindu", "Wai Wai", "iD", "Aachi", "Ganesh", "Ching's Secret", "Unibic", "Emami Healthy & Tasty", "Britannia", "Parle's Wafers", "Bambino", "Three Mango", "Cadbury Perk", "Dukes Waffy", "Durga", "Britannia Milk Bikis", "Haldiram's Prabhuji", "Little Heart", "Gold Winner", "Freedom", "Gold Drop", "Minute Maid", "Heritage", "Safe Harvest", "Navata", "Sunfeast Dream Cream", "Colgate Total", "Pran", "Star Gold", "Bemisal", "Milky Mist", "Gold Touch", "Uncle Chipps", "Shelly's", "Sunland", "Prabhat", "Cadbury Gems", "Tropicana Slice", "Horlicks", "Pringles", "Aashirvaad Svasti", "Mr. Gold", "Cheetos", "Nestle", "Cadbury", "Nandini", "Sunpure", "Anil", "Aadhaar", "Healthy Heart", "Britannia Little Hearts", "Tropicana Essentials", "Samrat", "Bauli", "Nova", "Parle", "MTR 3 Minute", "Del Monte", "McVitie's", "Parle Marie", "Elina", "Borges", "Parle Hide & Seek Fab", "Colgate Active Salt", "Bhagyalakshmi", "Dynamix", "Gopaljee Ananda", "Britannia Treat", "Milkfood", "Bingo Tedhe Medhe", "Dabur Hajmola", "Ferrero Rocher", "Cadbury Temptations", "Parle Kismi", "By Nature", "Rani"]
input_file = sys.argv[1]
with open(input_file) as f:
    temp_dict = {}
    for line in f:
        dict = json.loads(line)
        if dict.get('brand','').title() in brand_list:
            temp_dict['zip'] = dict['pincode']
            temp_dict['url'] = dict['url']
            temp_dict['pincode_s'] = dict['pincode']
            temp_dict['city_s'] = dict['city']
            temp_dict['city'] = dict['city']
            temp_dict['meta'] = dict.get('meta', 'NA')
            temp_dict['product_id'] = dict.get('product_id','')
            temp_dict['store'] = dict['store_name']
            fp.write(json.dumps(temp_dict))
            fp.write('\n')
