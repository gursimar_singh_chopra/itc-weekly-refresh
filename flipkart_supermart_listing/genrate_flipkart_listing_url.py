titles = ["24 Mantra", "24 Mantra Organic", "Aashirvaad", "Annapoorna", "B Natural", "Bingo", "Britannia", "Cadbury",
          "Cadbury Bournvita", "Ching's Secret", "Everest", "Fortune", "Haldiram", "Haldiram's", "Kitchens of India",
          "Knorr", "Kurkure", "Lay's", "Lotte", "Maggi", "Maggi chicken", "McVitie's", "McVities", "MTR", "Oreo",
          "Paper Boat", "Parle", "Patanjali", "Pillsbury", "Real", "Real B", "Real Basmati", "Samrat", "Sunfeast",
          "Top Ramen", "Tropicana", "Unibic", "Yippee"]

flipkart_supermart_listing = open('flipkart_supermart_listing_seed_file', 'w+')


for each_title in titles:
    url = 'https://www.flipkart.com/search?q=' + each_title + '&marketplace=GROCERY'
    flipkart_supermart_listing.write(url)
    flipkart_supermart_listing.write('\n')

flipkart_supermart_listing.close()
