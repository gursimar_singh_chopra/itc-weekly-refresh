import sys, json, csv

fp2 = {}
c1 = {}
c2 = {}
with open('pin_map.csv') as fe:
    reader = csv.reader(fe)
    for row in reader:
        if row[0].strip() not in fp2:
            fp2[row[0].strip()] = 1
            p1 = row[0].strip()
            p1 = open(row[0].strip() + ".json", 'a+')
        c1[row[0].strip()] = row[1].strip()
        c2[row[0].strip()] = row[2].strip()

dict_ = ["###zipcode:600017", "###zipcode:400012", "###zipcode:500033", "###zipcode:700005", "###zipcode:560064",
         "###zipcode:600042", "###zipcode:110065", "###zipcode:400097", "###zipcode:560038", "###zipcode:122009",
         "###zipcode:110024", "###zipcode:500080", "###zipcode:600090", "###zipcode:110070", "###zipcode:121001",
         "###zipcode:110091", "###zipcode:500072", "###zipcode:700071", "###zipcode:560100", "###zipcode:400067",
         "###zipcode:400050", "###zipcode:400005", "###zipcode:700106", "###zipcode:400053"]
for zips in dict_:
    print zips
    f1 = open(zips.split(':')[-1].strip() + ".json", 'w+')
    for each in open(sys.argv[1]):
        each = each.strip()
        ldata = {}
        ldata['source'] = "FlipkartSupermart-IN"
        ldata['url'] = each + zips
        ldata['zip'] = ldata['pincode_s'] = zips.split(':')[-1].strip()
        ldata['city_s'] = ldata['City'] = c2.get(ldata['zip'])
        ldata['area_s'] = ldata['Area'] = c1.get(ldata['zip'])
        print >> f1, json.dumps(ldata)
