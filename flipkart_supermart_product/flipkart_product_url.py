import json
import sys

fp = open('flipkart_product_seed_file.json', 'w+')

input_file = sys.argv[1]
with open(input_file) as f:
    temp_dict = {}
    for line in f:
        dict = json.loads(line)
        temp_dict['zip'] = dict['zip']
        temp_dict['url'] = dict['url']
        temp_dict['area_s'] = dict['area_s']
        temp_dict['pincode_s'] = dict['pincode_s']
        temp_dict['city_s'] = dict['city_s']
        fp.write(json.dumps(temp_dict))
        fp.write('\n')
