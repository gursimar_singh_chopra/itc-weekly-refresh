import json
import requests
import hashlib
import sys
import os
import datetime
import urllib
import csv
import time
import codecs


def createCrawlObject(crawl_options,filename):

	crawlobj={}
	crawlobj['jobtype']="split_crawl"
	crawlobj['username']="gursimar.chopra@dataweave.com"
	crawlobj['crawl_type']=crawl_options['crawl_type']
	crawlobj['periodic']="yes"
	crawlobj['fresh_data']=True
	crawlobj['source']=crawl_options['source']


	if 'maxThreads' in crawl_options:
		crawlobj['maxThreads']=crawl_options.get('maxThreads')

	if 'maxurlpersplit' in crawl_options:
		crawlobj['maxurlpersplit']=crawl_options.get('maxurlpersplit')

	if 'recrawl_failed_urls' in crawl_options:
		crawlobj['recrawl_failed_urls']=crawl_options.get('recrawl_failed_urls')


	if 'crawl_iteration' in crawl_options:
		crawlobj['crawl_iteration']=crawl_options.get('crawl_iteration')


	if 'ext_method' in crawl_options:
		crawlobj['ext_method']=crawl_options['ext_method']
	if 'no_db' in crawl_options:
		crawlobj['no_db']=crawl_options['no_db']
	if 'fresh_data' in crawl_options:
		crawlobj['fresh_data']=crawl_options['fresh_data']
	if 'priority' in crawl_options:
		crawlobj['priority']=0

	if 'proxy' in crawl_options:
		crawlobj['proxy']=crawl_options['proxy']
	if 'with_session'  in crawl_options:
		crawlobj['with_session']=1
	crawlobj['userid']=""
	crawlobj['hour']="01"
	crawlobj['minute']="00"
	crawlobj['dow']="*"
	crawlobj['pip_mode']="1"
	crawlobj['captcha']="1"

	if 'get_all' in crawl_options and crawl_options['get_all']==1:
		crawlobj['get_all']="1"
	else:
		crawlobj['not_crawled'] = "1"
		crawlobj['get_all'] = 0

	file1 = {'file1': open(filename, 'rb')}
	crawlobj['file1']=file1
	crawlobj['queueid']="0"

	crawlobj['delay']=""

	crawlobj['tracking_code'] = crawl_options['tracking_code']
	if 'seller' in crawl_options:
		crawlobj['seller'] = 1
	if 'no_plugin' in crawl_options:
		crawlobj['no_plugin'] = 1
	if 'variant' in crawl_options:
		crawlobj['variant'] = 1

	crawlobj['copyjson']=1
	crawlobj["email_alert"]="1"

	return crawlobj


if __name__ == '__main__':
	
	crawl_options={}

	crawl_options["source"]='FlipkartSupermart-IN'
	crawl_options['crawl_type']='product'
	crawl_options["maxurlpersplit"]="100"
	crawl_options["priority"]=0
	crawl_options["with_session"]=1
	crawl_options['fresh_data']='true'
	crawl_options["tracking_code"]="ITC_Brands_Competitor_FlipkartSupermart_0224_52"
	#crawl_options['no_plugin']=1
	crawl_options['variant ']=1
	lt = ["110024.json","110065.json","110070.json","110091.json","121001.json","122009.json","400005.json","400012.json","400050.json","400053.json","400067.json","400097.json","500033.json","500072.json","500080.json","560038.json","560064.json","560100.json","600017.json","600042.json","600090.json","700005.json","700071.json","700106.json"]
	for each in lt:
		crawlobj=createCrawlObject(crawl_options,each.strip())
		new_api="http://scheduler.dweave.net/inputapi"
		data_schedule=requests.post(new_api,data=(crawlobj),files=crawlobj['file1']).json()
		print crawlobj
		print data_schedule
		print new_api
		time.sleep(2)
	

