import json
import sys

fp = open((sys.argv[1].replace('.json',''))+'_product_seed_file.json', 'w+')
brand_list = ["24 MANTRA", "AASHIRVAAD", "AASHIRVAAD SVASTI", "AMUL", "AMUL GOLD", "AMUL HAPPY TREATS", "AMUL MOTI", "AMUL PRO", "AMUL SLIM", "AMUL TAAZA", "AMULYA", "ANNAPOORNA", "ANNAPURNA", "BB POPULAR", "BINGO", "B NATURAL", "BRITANNIA", "BRITANNIA BREAD", "BRITANNIA DAILY FRESH", "CADBURY", "CADBURY BOURNVILLE", "CADBURY BOURNVITA", "CADBURY CELEBRATIONS", "CADBURY DAIRY MILK", "CADBURY DAIRY MILK SILK", "CADBURY FUSE", "CADBURY GEMS", "CADBURY PERK", "CADBURY SILK", "CADBURY TEMPTATIONS", "CANDYMAN", "CHINGS", "CHING'S SECRET", "EVEREST", "FORTUNE", "GANESH", "GANESH BHEL", "GANESH PAPAD", "HALDIRAM", "HALDIRAM BHUJIAWALA", "HALDIRAMS", "KITCHENS OF INDIA", "KNORR", "KURKURE", "LAYS", "LOTTE", "MAGGI", "MTR", "MTR FOODS", "NATURE FRESH", "OREO", "PAPER BOAT", "PARLE", "PARLE-G", "PATANJALI", "PILLSBURY", "REAL", "REAL ACTIV", "REAL GOOD", "REAL GOOD YUMMIEZ", "REAL THAI", "SAMRAT", "SUNFEAST", "SUNFEAST YIPPEE!", "TIGER", "TIGER BALM", "TOP RAMEN", "TROPICANA", "UNIBIC"]
input_file = sys.argv[1]
with open(input_file) as f:
    temp_dict = {}
    for line in f:
        dict = json.loads(line)
        if dict.get('brand','').upper() in brand_list:
            temp_dict['category'] = dict['sub_category_1']
            temp_dict['url'] = dict['url']
            # temp_dict['product_type'] = dict['product_type']
            temp_dict['city_s'] = dict['city_s']
            temp_dict['zip'] = dict['zip']
            temp_dict['title'] = dict.get('title', 'NA')
            temp_dict['brand'] = dict.get('brand','')
            temp_dict['pincode'] = dict['pincode']
            # temp_dict['area'] = dict['area_s']
            temp_dict['pincode_s'] = dict.get('pincode_s', 'NA')
            temp_dict['subcategory'] = dict.get('sub_category_2', '')
            # temp_dict['thumbnail'] = dict['thumbnail']

            fp.write(json.dumps(temp_dict))
            fp.write('\n')
