import json
import requests
import hashlib
import sys
import os
import datetime
import urllib
import csv
import time
import codecs


def createCrawlObject(crawl_options,filename):

	crawlobj={}
	crawlobj['jobtype']="split_crawl"
	crawlobj['username']="gursimar.chopra@dataweave.com"
	crawlobj['crawl_type']=crawl_options['crawl_type']
	crawlobj['periodic']="yes"
	crawlobj['source']=crawl_options['source']


	if 'maxThreads' in crawl_options:
		crawlobj['maxThreads']=crawl_options.get('maxThreads')

	if 'maxurlpersplit' in crawl_options:
		crawlobj['maxurlpersplit']=crawl_options.get('maxurlpersplit')

	if 'recrawl_failed_urls' in crawl_options:
		crawlobj['recrawl_failed_urls']=crawl_options.get('recrawl_failed_urls')


	if 'crawl_iteration' in crawl_options:
		crawlobj['crawl_iteration']=crawl_options.get('crawl_iteration')


	if 'ext_method' in crawl_options:
		crawlobj['ext_method']=crawl_options['ext_method']
	if 'no_db' in crawl_options:
		crawlobj['no_db']=crawl_options['no_db']
	if 'priority' in crawl_options:
		crawlobj['priority']=0

	if 'proxy' in crawl_options:
		crawlobj['proxy']=crawl_options['proxy']
	if 'with_session'  in crawl_options:
		crawlobj['with_session']=1
	crawlobj['userid']=""
	crawlobj['hour']="01"
	crawlobj['minute']="00"
	crawlobj['dow']="*"

	if 'get_all' in crawl_options and crawl_options['get_all']==1:
		crawlobj['get_all']="1"
	else:
		crawlobj['not_crawled'] = "1"
		crawlobj['get_all'] = 0

	file1 = {'file1': open(filename, 'rb')}
	crawlobj['file1']=file1
	crawlobj['queueid']="0"
	crawlobj['depth']="2"

	crawlobj['delay']=""

	crawlobj['tracking_code'] = crawl_options['tracking_code']
	if 'seller' in crawl_options:
		crawlobj['seller'] = 1
	if 'variant' in crawl_options:
		crawlobj['variant'] = 1
	if 'fresh_data' in crawl_options:
        	crawlobj['fresh_data'] = crawl_options['fresh_data']
	crawlobj['copyjson']=1
	crawlobj["email_alert"]="1"

	return crawlobj


if __name__ == '__main__':
	
	crawl_options={}

	
	crawl_options['crawl_type']='product'
	crawl_options["maxurlpersplit"]="300"
	crawl_options["maxThreads"]=40
	crawl_options["priority"]=0
	crawl_options["with_session"]=1
	crawl_options["tracking_code"]="ITC_Brands_Competitor_BigBasket_final_17"
	crawl_options['fresh_data'] = 'true'
	crawl_options['proxy']='blazing'
	lt = {"110024.json": "BigBasket-DEL","110065.json": "BigBasket-DEL","110070.json": "BigBasket-DEL","110091.json": "BigBasket-DEL","121001.json":"BigBasket-GUR","122009.json": "BigBasket-GUR","400005.json": "BigBasket-MUM","400012.json": "BigBasket-MUM","400050.json": "BigBasket-MUM","400053.json": "BigBasket-MUM","400067.json": "BigBasket-MUM","400097.json": "BigBasket-MUM","500033.json": "BigBasket-HYD","500072.json": "BigBasket-HYD","500080.json": "BigBasket-HYD","560038.json": "BigBasket-BLR","560064.json": "BigBasket-BLR","560100.json": "BigBasket-BLR","600017.json": "BigBasket-CHE","600042.json": "BigBasket-CHE","600090.json": "BigBasket-CHE","700005.json": "BigBasket-KOL","700071.json": "BigBasket-KOL","700106.json": "BigBasket-KOL"}
	
	#lt = {"110024.json": "BigBasket-DEL","121001.json": "BigBasket-GUR","400005.json": "BigBasket-MUM","500033.json": "BigBasket-HYD","560038.json": "BigBasket-BLR","600017.json": "BigBasket-CHE","700005.json": "BigBasket-KOL"}
	for each,src in lt.iteritems():
		crawl_options["source"]=src
		crawlobj=createCrawlObject(crawl_options,each.strip())
		new_api="http://scheduler.dweave.net/inputapi"
		data_schedule=requests.post(new_api,data=(crawlobj),files=crawlobj['file1']).json()
		print crawlobj
		print data_schedule
		print new_api
		time.sleep(2)
	

