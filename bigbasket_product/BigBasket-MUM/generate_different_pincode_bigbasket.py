import json
import sys

input_file = sys.argv[1]
with open(input_file) as f:
    temp_dict = {}
    for line in f:
        dict = json.loads(line)
        pincode = dict['pincode_s']
        if pincode not in temp_dict:
            temp_dict[pincode] = []
        temp_dict[pincode].append(dict)


for each_pincode in temp_dict.keys():
    fp = open((str(each_pincode)+ '.json'),'w+')

    for each_json in temp_dict[each_pincode]:
        fp.write(json.dumps(each_json))
        fp.write('\n')
    fp.close()


print(temp_dict.keys())
print(len(temp_dict.keys()))

