import json, sys, csv

count = 0


def WriteHeaders(headers):
    cfile = open(sys.argv[2], 'w+')
    csvfile = csv.writer(cfile)
    csvfile.writerow(headers)
    return csvfile


def hande_encode_data(value):
    nvalue = ''
    try:
        nvalue = str(value)
    except Exception, e:
        navlue = ''

    return nvalue


def generate_report(file1, fp):
    count = 0
    for data in open(file1):
        towrite = []
        try:
            ldata = json.loads(data)
        except:
            count += 1
            pass
        try:

            if 'sku' in ldata:
                towrite.append(ldata.get('source'))
                towrite.append(ldata.get('sku'))
                towrite.append(ldata.get('brand'))
                towrite.append(ldata.get('title'))
                towrite.append(ldata.get('sub_category_1'))
                towrite.append(ldata.get('sub_category_2'))
                towrite.append(ldata.get('sub_category_3'))
                towrite.append(ldata.get('available_price'))
                towrite.append(ldata.get('mrp'))
                towrite.append(ldata.get('city_s'))
                towrite.append(ldata.get('pincode_s'))
                towrite.append(ldata.get('express_delivery'))
                towrite.append(ldata.get('url'))
                tmpwrite = []
                for listdata in towrite:
                    tmpwrite.append(hande_encode_data(listdata))
                fp.writerow(tmpwrite)
        except:
            pass


if __name__ == '__main__':
    input_file = sys.argv[1]
    headers = ["Source", "SKU","Brand", "Title","Sub Category 1","Sub Category 2","Sub Category 3","Available Price",'MRP',"City", "Pincode","express_delivery", "URL"]
    fp = WriteHeaders(headers)
    generate_report(input_file, fp)