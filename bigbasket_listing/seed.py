import sys, json, csv

with open(sys.argv[1]) as fp:
    reader = csv.reader(fp)
    next(reader, None)
    for row in reader:
        name_of_file = str(row[0].strip())
        f1 = open(str(row[0].strip()) + ".json", 'w+')
        with open(sys.argv[2]) as seed_file:
            big_basket_reader = csv.reader(seed_file)
            next(big_basket_reader, None)
            for big_basket_row in big_basket_reader:
                category = str(big_basket_row[0].strip())
                sub_category_1 = str(big_basket_row[1].strip())
                sub_category_2 = str(big_basket_row[2].strip())
                sub_category_3 = str(big_basket_row[3].strip())
                url = str(big_basket_row[4].strip())
                obj = {}
                obj['sub_category_1'] = sub_category_1
                obj['sub_category_2'] = sub_category_2
                obj['sub_category_3'] = sub_category_3
                obj['url'] = url+'#!#zipcode:'+str(str(row[0].strip()))
                obj['pincode_s'] = str(row[0].strip())
                obj['zip'] = obj['pincode'] = str(row[0].strip())
                #obj['city_s'] = str(row[1].strip())
		obj['city_s'] = str(row[2].strip())
                print >> f1, json.dumps(obj)
        f1.close()
